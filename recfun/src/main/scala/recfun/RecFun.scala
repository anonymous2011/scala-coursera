package recfun

object RecFun extends RecFunInterface {

  def main(args: Array[String]): Unit = {
//    println("Pascal's Triangle")
//    for (row <- 0 to 10) {
//      for (col <- 0 to row)
//        print(s"${pascal(col, row)} ")
//      println()
//    }
//    val str : String = "(:()(()"
//    val input : List[Char] = str.toList
//    for (e <- input) print(e + " ")
//    println()
//    println(balance(input))
//    println(countChange(10, List(1, 2, 3)))
    println(pascal(0, 2))
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    def findByLevel(r : Int) : Array[Int] = {
      if (r == 1) Array(1, 1)
      else if (r == 0) Array(1)
      else {
        val pre = findByLevel(r - 1)
        val curr = new Array[Int](r + 1)
        curr(0) = 1
        for (i <- 1 until r) {
          curr(i) = pre(i) + pre(i - 1)
        }
        curr(r) = 1
        curr
      }
    }
    findByLevel(r)(c)
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    def check(index: Int, count : Int, chars: List[Char]) : Boolean = {
      if (index == chars.size) count == 0
      else if (chars(index) == '(') check(index + 1, count + 1, chars)
      else if (chars(index) == ')') count > 0 && check(index + 1, count - 1, chars)
      else check(index + 1, count, chars)
    }
    check(0, 0, chars)
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
    def search(money: Int, coins: List[Int]) : Int = {
      if (coins.isEmpty || money < 0) 0
      else if (money == 0) 1
      else search(money - coins.head, coins) + search(money, coins.tail)
    }
    search(money, coins)
  }
}
